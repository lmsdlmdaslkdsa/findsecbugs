#
# Download latest versions
#
FROM busybox:latest as downloader

# Setup
ARG SPOTBUGS_VERSION
ARG FINDSECBUGS_VERSION

# Download latest versions
RUN wget -q -O - http://repo.maven.apache.org/maven2/com/github/spotbugs/spotbugs/${SPOTBUGS_VERSION}/spotbugs-${SPOTBUGS_VERSION}.tgz | tar -xzf - -C /tmp; \
    mv /tmp/spotbugs-* /tmp/spotbugs; \
    wget -q -O - https://search.maven.org/remotecontent?filepath=com/h3xstream/findsecbugs/findsecbugs-plugin/${FINDSECBUGS_VERSION}/findsecbugs-plugin-${FINDSECBUGS_VERSION}.jar >/tmp/spotbugs/plugin/findsecbugs-plugin.jar


#
# Target container
#
FROM openjdk:8-jre-slim

# Setup
ARG SPOTBUGS_VERSION
ARG FINDSECBUGS_VERSION


# Documentation
LABEL maintainer="devsecops@bellmedia.ca"
LABEL SPOTBUGS_VERSION=${SPOTBUGS_VERSION}
LABEL FINDSECBUGS_VERSION=${FINDSECBUGS_VERSION}

# Install SpotBugs
COPY --from=downloader /tmp/spotbugs /usr/local/spotbugs
